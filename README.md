# derouleur-types

Ce projet a pour but de fournir des structures de typage, possiblement documentées, pour le fichier [derouleur.json](http://www2.assemblee-nationale.fr/static/derouleur/derouleur.json) fourni par l'assemblée nationale. 

Les types sont construits à partir d'échantillons du fichier json récupéré à différentes dates. 

## Installation des types

Le fichier de typage est disponible via le registre npmjs et récupérable en tapant : `npm i derouleur-types` à la racine de votre projet. 

## Utilisation des types fournis

Vous pouvez désormais importer les types dans votre projet typescript : 

```
    import { Derouleur, Phase, Ligne } from 'derouleur-types';
```

## Installation du générateur

derouleur-types utilise `node` pour générer les différents types à travers la librairie `quicktype`

Vous pouvez également utiliser cette librairie en tant que générateur pour un ensemble de langages listés plus bas.

Pour cela clonez le projet : 
```
    git clone https://framagit.org/Asone/derouleur-types.git`
```
puis installez la dépendance nécéssaire : 
```
    npm i
```

## Génération des types

la récupération et compilation du flux `derouleur.json` pour en générer les définitions de types est disponible pour les langages suivants : 

- Flowtypes: `npm run build:flow`
- Java: `npm run build:java` 
- Ruby: `npm run build:ruby`
- Rust: `npm run build:rust`
- Typescript: `npm run build:typescript`

Le résultat de l'extraction est disponible dans le dossier `dist`


## Structure 

Le dépôt du projet contient deux dossiers : 

- `dist` est le dossier où par défaut les fichiers générés seront stockés
- `legacy` est le dossier qui a pour vocation à exposer les définitions consolidées. Ce sont ces interfaces qui sont exposées en tant que module tiers. 

## Roadmap

L'outil en l'état, s'il peut être utile à l'exploitation, est très limitatif. En effet, le flux du dérouleur peut ne pas exposer nécéssairement l'ensemble des champs de données disponibles. 

Pour réduire l'incompatibilité entre l'exploitation du flux avec ses structures de typage, il faudra faire évoluer l'outil pour mettre à disposition des structures consolidées au fil de plusieurs itérations.

Dans un premier temps ce travail peut se faire manuellement, en configurant le dépôt et le projet npm afin de pointer vers les fichiers consolidés. 

Idéalement, à terme l'outil serait automatisé afin : 

- d'extraire les types dans des fichiers séparés par déclaration d'interface.
- de consolider automatiquement les données en faisant un diff entre la version consolidée actuelle et un flux mis à jour. 


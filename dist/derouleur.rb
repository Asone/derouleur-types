require 'json'
require 'dry-types'
require 'dry-struct'

module Types
  include Dry::Types.module

  Hash                     = Strict::Hash
  String                   = Strict::String
  LigneAmdtsIdentiquesEnum = Strict::String.enum("debut", "fin", "milieu")
  LigneLibelleCompl1       = Strict::String.enum("(supprimé)")
  LigneType                = Strict::String.enum("ADT", "ARTICLE", "INSCRIT", "INSCRITDG", "INTERVCOMMISS", "INTERVGVT", "LIBRE", "MOTION", "NEXTSEANCEL1", "NEXTSEANCEL2", "NEXTSEANCEL3", "REPORTVOTE", "SSADT")
end

module LigneAmdtsIdentiquesEnum
  Debut  = "debut"
  Fin    = "fin"
  Milieu = "milieu"
end

module LigneLibelleCompl1
  Supprimé = "(supprimé)"
end

module LigneType
  ADT           = "ADT"
  Article       = "ARTICLE"
  Inscrit       = "INSCRIT"
  Inscritdg     = "INSCRITDG"
  Intervcommiss = "INTERVCOMMISS"
  Intervgvt     = "INTERVGVT"
  Libre         = "LIBRE"
  Motion        = "MOTION"
  Nextseancel1  = "NEXTSEANCEL1"
  Nextseancel2  = "NEXTSEANCEL2"
  Nextseancel3  = "NEXTSEANCEL3"
  Reportvote    = "REPORTVOTE"
  Ssadt         = "SSADT"
end

class Ligne < Dry::Struct
  attribute :id,                                        Types::String
  attribute :ligne_type,                                Types::LigneType
  attribute :texte_bibard,                              Types::String
  attribute :ligne_libelle_1,                           Types::String
  attribute :ligne_video_highlighted,                   Types::String.optional
  attribute :depute_tribun_id,                          Types::String.optional
  attribute :ligne_duree,                               Types::String.optional
  attribute :ligne_libelle_2,                           Types::String.optional
  attribute :ligne_libelle_3,                           Types::String.optional
  attribute :ligne_amendement_derouleur_division_ancre, Types::String.optional
  attribute :ligne_amendement_uid,                      Types::String.optional
  attribute :ligne_amdts_identiques,                    Types::LigneAmdtsIdentiquesEnum.optional
  attribute :ligne_dc_amdts,                            Types::LigneAmdtsIdentiquesEnum.optional
  attribute :ligne_libelle_compl_1,                     Types::LigneLibelleCompl1.optional
end

class Phase < Dry::Struct
  attribute :phase_libelle, Types::String
  attribute :phase_type,    Types::String
  attribute :ligne,         Types.Array(Ligne)
end

class Contenu < Dry::Struct
  attribute :phase, Types.Array(Phase)
end

class Jaune < Dry::Struct
  attribute :id,                Types::String
  attribute :jaune_date_time,   Types::String
  attribute :extract_date_time, Types::String
end

class Racine < Dry::Struct
  attribute :jaune,   Jaune
  attribute :contenu, Contenu
end

class Derouleur < Dry::Struct
  attribute :racine, Racine
end

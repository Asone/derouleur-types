export interface Derouleur{
    racine: Racine;
}

export interface Racine {
    jaune:   Jaune;
    contenu: Contenu;
}

export interface Contenu {
    phase: Phase[] ;
}

export interface Phase {
    phase_libelle: string;
    phase_type:    string;
    ligne:         Ligne[];
}

export interface Ligne {
    id:                                         string;
    ligne_type:                                 LigneType;
    texte_bibard:                               string;
    ligne_libelle_1:                            string;
    ligne_video_highlighted?:                   string;
    depute_tribun_id?:                          string;
    ligne_duree?:                               string;
    ligne_libelle_2?:                           string;
    ligne_libelle_3?:                           string;
    ligne_amendement_derouleur_division_ancre?: string;
    ligne_amendement_uid?:                      string;
    ligne_amdts_identiques?:                    LigneAmdtsIdentiquesEnum;
    ligne_dc_amdts?:                            LigneAmdtsIdentiquesEnum;
    ligne_libelle_compl_1?:                     LigneLibelleCompl1;
}

export enum LigneAmdtsIdentiquesEnum {
    Debut = "debut",
    Fin = "fin",
    Milieu = "milieu",
}

export enum LigneLibelleCompl1 {
    Supprimé = "(supprimé)",
}

export enum LigneType {
    ADT = "ADT",
    Article = "ARTICLE",
    Inscrit = "INSCRIT",
    Inscritdg = "INSCRITDG",
    Intervcommiss = "INTERVCOMMISS",
    Intervgvt = "INTERVGVT",
    Libre = "LIBRE",
    Motion = "MOTION",
    Nextseancel1 = "NEXTSEANCEL1",
    Nextseancel2 = "NEXTSEANCEL2",
    Nextseancel3 = "NEXTSEANCEL3",
    Reportvote = "REPORTVOTE",
    Ssadt = "SSADT",
}

export interface Jaune {
    id:                string;
    jaune_date_time:   Date;
    extract_date_time: Date;
}
